import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'
import Cookie from 'js-cookie'

function lazyLoading(componentPath) {
    return () => import(`@/pages/${componentPath}`)
}

Vue.use(Router)
const router = new Router({
  mode: "history",
  routes: [
    { 
      path: '/', 
      name: 'Home', 
      redirect: '/existingCourse'
    },
    {
      path: '/course/createCourse',
      name: "CreateCourse",
      component: lazyLoading('course/create-course'),
      meta: {
        showHomeIcon: 'home'
      }
    },
    {
      path: '/course/createQuestions',
      name: "createQuestions",
      component: lazyLoading('course/create-questions'),
      meta: {
        showHomeIcon: 'home'
      }
    },
    {
      path: '/course/index',
      name: 'CourseIndex',     
      component: lazyLoading('course/index'),
      meta: {
        pathName: "课程/主页",
        path: [
          '',
          '/course/index'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: '/course/DataAnalysis',
      name: 'DataAnalysis',     
      component: lazyLoading('course/data-analysis'),
      meta: {
        pathName: "课程/主页",
        path: [
          '',
          '/course/index'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: '/course/AssignmentMark',
      name: 'AssignmentMark',     
      component: lazyLoading('course/assignment-mark'),
      meta: {
        pathName: "课程/主页",
        path: [
          '',
          '/course/index'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: '/course/mode',
      name: 'EditMode',     
      component: lazyLoading('course/edit-mode'),
      meta: {
        pathName: "",
        path: [
          '',
          '/course/index'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: '/account',
      name: 'Account',
      component: lazyLoading('account'),
      meta: {
        pathName: "账户/个人主页",
        path: [
          '',
          '/account'
        ]
      }
    },
    {
      path: '/assist',
      name: 'assist',
      component: lazyLoading('svg-icon-list')
    },
    {
      path: '/performance',
      name: 'performance',
      component: lazyLoading('performance')
    },
    {
      path: '/stu-performance',
      name: 'StudentPerformance',
      component: lazyLoading('student-performance'),
      meta: {}
    },
    {
      path: '/addCourse',
      name: 'addCourse',
      component: lazyLoading('course/add-course'),
      meta: {
        pathName: "课程/添加课程",
        path: [
          '',
          '/addCourse'
        ]
      },
    },
    {
      path: '/existingCourse',
      name: 'existingCourse',
      component: lazyLoading('course/existing-courses'),
      meta: {
        pathName: "课程/已有课程",
        path: [
          '',
          '/existingCourse'
        ]
      }
    },
    {
      path: '/educationalManagement',
      name: 'educationalManagement',
      component: lazyLoading('course/educational-management'),
      meta: {
        pathName: "教务管理",
        path: [
          '/educationalManagement'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: '/login',
      name: "login",
      component: lazyLoading('login')
    },
    {
      path: '/correct',
      name: "Correct",
      component: lazyLoading('correct/correct-draw'),
      meta: {
        pathName: "",
        path: [
          '/educationalManagement'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: '/correct-test',
      name: "CorrectTest",
      component: lazyLoading('correct/correct-test'),
      meta: {
        pathName: "",
        path: [
          '/educationalManagement'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: "/calendar",
      name: "Calendar",
      component: lazyLoading('calendar/calendar')
    },
    {
      path: "/event/list",
      name: "EventList",
      component: lazyLoading('calendar/event_list')
    },
    {
      path: "/data/comparison",
      name: "DataComparison",
      component: lazyLoading('course/data-comparison'),
      meta: {
        showHomeIcon: 'home'
      }
    },
    {
      path: "/examination",
      name: "Examination",
      component: lazyLoading('correct/examination'),
      meta: {
        pathName: "",
        path: [
          '/educationalManagement'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: "/dataAnalysisInner",
      name: "DataAnalysisInner",
      component: lazyLoading('course/data-analysis-inner')
    },
    {
      path: "/message",
      name: "Message",
      component: lazyLoading('message/message'),
      meta: {
        pathName: "消息",
        path: [
          '/message'
        ]
      }
    },
    {
      path: "/CreateExamine",
      name: "CreateExamine",
      component: lazyLoading('course/create-examine'),
      meta: {
        pathName: "",
        path: [
          '/educationalManagement'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: "/class/index",
      name: "ClassIndex",
      component: lazyLoading('classes/class-index'),
    },
    {
      path: "/class/management",
      name: "ClassManagement",
      component: lazyLoading('classes/class-management')
    },
    {
      path: "/class/student",
      name: "ClassStudent",
      component: lazyLoading('classes/student-management')
    },
    {
      path: "/class/score",
      name: "ClassScore",
      component: lazyLoading('classes/score-management'),
      // meta: {
      //   pathName: "",
      //   path: [
      //     '/educationalManagement'
      //   ],
      //   showHomeIcon: 'home'
      // }
    },
    {
      path: "/class/score/entry",
      name: "ClassScoreEntry",
      component: lazyLoading('classes/entry-grade'),
    },
    {
      path: '/class/existingCourse',
      name: 'ClassCourse',
      component: lazyLoading('course/existing-courses'),
      meta: {
        pathName: "班级/课程",
        path: [
          '/class/management',
          '/class/existingCourse'
        ]
      }
    },
    {
      path: "/courseware",
      name: "Courseware",
      component: lazyLoading('course/create-courseware'),
      meta: {
        pathName: "",
        path: [
          '/educationalManagement'
        ],
        showHomeIcon: 'home'
      }
    },
    {
      path: '/correct-exam',
      name: 'correctExam',
      component: lazyLoading('correct/correct-exam'),
      meta: {
        showHomeIcon: 'home'
      }
    },
    {
      path: "/correctDraw",
      name: "correctDraw",
      component: lazyLoading('correct/correct-draw')
    },
    {
      path: "/previewPDF",
      name: "previewPDF",
      component: lazyLoading("correct/preview")
    },

        //学生端
        {
            path: '/stu/management',
            name: 'studentManagement',
            component: lazyLoading('student/student-management'),
        },
        {
            path: '/stu/assignment',
            name: 'studentAssignment',
            component: lazyLoading('student/student-assignment'),
            meta: {
                pathName: '',
                path: ['/educationalManagement'],
                showHomeIcon: 'home',
            },
        },
        {
            path: '/stu/courseware',
            name: 'studentCourseware',
            component: lazyLoading('student/student-courseware'),
            meta: {
                pathName: '',
                path: ['/educationalManagement'],
                showHomeIcon: 'home',
            },
        },
        {
            path: '/stu/assignment/draw',
            name: 'studentAssignmentDraw',
            component: lazyLoading('correct/correct-draw-student'),
            meta: {
                pathName: '',
                path: ['/educationalManagement'],
                showHomeIcon: 'home',
            },
        },
        {
            path: '/questionLib',
            name: 'questionLib',
            component: lazyLoading('question-lib'),
        },
        {
            path: '/quiz',
            name: 'quiz',
            component: lazyLoading('quiz/index'),
        },
        {
            path: '/view/mode',
            name: 'viewAssignmentExam',
            component: lazyLoading('course/view-assign-exam'),
            meta: {
              pathName: "",
              path: [
                '/educationalManagement'
              ],
              showHomeIcon: 'home'
            }
        },
    ],
})

router.beforeEach((to, from, next) => {
    const token = Cookie.get('edu_token') || '';
    if (token) {
        if (to.name != from.name) {
            store.commit('setRouter', to.name)
            next()
        }
    } else {
        if (to.name != 'login') {
            router.push({ name: 'login' })
            next()
        } else {
            next()
        }
    }
})
// router.afterEach((to, from) => {
//   store.commit('setBreadcrumb', to.name)
// })
router.onError((err) => {
    console.log(err)
})
export default router
