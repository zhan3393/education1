import Vue from 'vue'
import Vuex from 'vuex'

import * as quiz from './quiz'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: '',
        router: '',
        user: {},
        classes: [],
        breadcrumb: {
            pathName: '',
            path: []
        },
        examIo: null
    },
    getters: {},
    mutations: {
        setToken(state, token) {
            state.token = token || ''
        },
        setRouter(state, routerName) {
            state.router = routerName || ''
        },
        setUser(state, user) {
            state.user = user || {}
        },
        setClasses(state, class_list) {
            state.classes = class_list || []
        },
        setBreadcrumb(state, data) {
            state.breadcrumb.pathName = data.name || ""
            state.breadcrumb.path = data.path || []
        },
        setExamIo(state, fn) {
            state.examIo = fn || null
        }
    },
    modules: {
        quiz
    }
})
