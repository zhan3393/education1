import socketIo from '@/http/socketIo'

const initialState = {
  questions: [],
  pages: [],
  performance: [],
  pptName: '',
  isCheckinStarted: false,
  isCheckinStopped: false,
  currentClass: {
    qrcode: '',
    scope: 100,
    location: null,
    classId: '',
    className: '',
    testClassId: '',
  },
  socket: null,
  questionProgress: { finished: 0, total: 0 },
  questionRatio: {},
  presentProgress: { present: 0, total: 0 },
}

export const state = () => ({ ...initialState })

export const mutations = {
  reset(state) {
    for (let key in initialState) {
      state[key] = initialState[key]
    }
  },
  setQuestions(state, questions) {
    state.questions = questions
  },
  setPages(state, { pages, ppt, performance }) {
    state.pages = pages
    state.pptName = ppt.name || ''
    state.performance = performance || []
  },
  setPerformance(state, performance) {
    state.performance = performance || []
  },
  startCheckin(state) {
    state.isCheckinStarted = true
    state.isCheckinStopped = false
  },
  stopCheckin(state) {
    state.isCheckinStarted = false
    state.isCheckinStopped = true
  },
  setTestClass(state, currentClass) {
    state.currentClass = currentClass
  },
  setTestClassQrcode(state, qrcode) {
    state.currentClass.qrcode = qrcode
  },
  setTestClassId(state, classId) {
    state.currentClass.classId = classId
  },
  setTestScope(state, scope) {
    state.currentClass.scope = scope
  },
  setTestClassName(state, className) {
    state.currentClass.className = className
  },
  setTestClassLocation(state, location) {
    state.currentClass.location = location
  },
  setScoket(state, socket) {
    state.socket = socket
  },
  changeQuestionProgress(state, data) {
    state.questionProgress.finished = data.finished
    state.questionProgress.total = data.total
  },
  changeQuestionRatio(state, data) {
    state.questionRatio = data || {}
  },
  changePresentProgress(state, data) {
    state.presentProgress.present = data.present
    state.presentProgress.total = data.total
  },
}

export const actions = {
  startSocket({ commit, dispatch, state }) {
    if (state.socket) {
      dispatch('stopSocket')
    }

    const id = state.currentClass.testClassId
    const socket = socketIo({
      url: 'ws.hanlinsys.com/mini',
      query: {
        room: `test-${id}`, // 房间-测试班级ID
        testClass: `${id}`, // 测试班级ID
      },
    })

    // 答题进度
    socket.on('questionProgress', (data) => {
      commit('changeQuestionProgress', data)
    })

    socket.on('present', (data) => {
      commit('changePresentProgress', data)
    })

    commit('setScoket', socket)
  },
  stopSocket({ state, commit }) {
    if (state.socket) {
      state.socket.disconnect()
      commit('setScoket', null)
    }
  },
  startQuestionSocket({ state }, questionId) {
    if (state.socket) {
      state.socket.emit('startQuestion', {
        question: questionId, // 题目ID
      })
    }
  },
  closeQuestionSocket({ state }, questionId) {
    if (state.socket) {
      state.socket.emit('closeQuestion', {
        question: questionId, // 题目ID
      })
    }
  },

  async queryQuestions({ commit }, testId) {
    const where = JSON.stringify({ test: testId })
    const { data } = await this.$api.Test.get_test_questions({
      current: 1,
      pageSize: 99999,
      where,
    })
    const questions = data.list
      .filter((x) => x.page)
      .sort((a, b) => {
        if (a.page === b.page) {
          return a.questionNo - b.questionNo
        } else {
          return a.page - b.page
        }
      })
    commit('setQuestions', questions)
  },

  async queryTest({ commit }, testId) {
    const { data } = await this.$api.Test.query_test(testId)
    const { ppt = {}, pages = [], performance = [] } = data
    commit('setPages', { pages, ppt, performance })
  },

  async startCheckin({ commit, state, dispatch }) {
    commit('startCheckin')
    dispatch('startSocket')
    await this.$api.Test.start_present(state.currentClass.testClassId)
    state.socket.emit('startPresent')
    this.$message.info('开始考勤')
  },
  async stopCheckin({ commit, state }) {
    commit('stopCheckin')
    await this.$api.Test.close_present(state.currentClass.testClassId)
    state.socket.emit('closePresent')
    this.$message.info('结束考勤')
  },
  async createTestClass({ commit }, { scope, location, classes, test }) {
    const result = await this.$api.Test.create_test_class({
      scope,
      location,
      classes,
      test,
    })
    if (result.data) {
      const { qrcode, scope = 100, location, _id } = result.data
      commit('setTestClass', {
        ...state.currentClass,
        scope,
        qrcode,
        location,
        testClassId: _id,
      })
    } else {
      this.$message.error(result.error)
    }
  },
  async queryTestClass({ commit, state }, { test, classes }) {
    const result = await this.$api.Test.query_test_classes({
      current: 1,
      pageSize: 9999,
      where: JSON.stringify({ test, classes }),
    })
    if (result.data.list && result.data.list.length) {
      const { qrcode, scope = 100, location, _id } = result.data.list[0]
      commit('setTestClass', {
        ...state.currentClass,
        scope,
        qrcode,
        location,
        testClassId: _id,
      })
    } else {
      commit('setTestClass', {
        classId: '',
        className: '',
        scope: 100,
        qrcode: '',
        location: null,
        testClassId: '',
      })
    }
  },

  async setPerformance({ commit }, { id, params }) {
    await this.$api.Test.set_test_performance(id, params)
    commit('setPerformance', params.performance)
  },

  async startQuestion({ state, dispatch }, questionId) {
    const params = { question: questionId }
    await this.$api.Test.start_question(state.currentClass.testClassId, params)
    dispatch('startQuestionSocket', questionId)
  },

  async closeQuestion({ state, dispatch, commit }, questionId) {
    const params = { question: questionId }
    const result = await this.$api.Test.close_question(
      state.currentClass.testClassId,
      params,
    )
    if (result.data && result.data.answerRatio) {
      commit('changeQuestionRatio', result.data.answerRatio)
    }
    dispatch('closeQuestionSocket', questionId)
  },
}

export const getters = {}

export const namespaced = true
