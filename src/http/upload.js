// import Http from './http'
// import store from '../store/index'
import StaticApi from './api/static'
export default function (params) {
    return new Promise((resolve, reject) => {
        let StaticApiFn = new StaticApi()
        StaticApiFn.getUploadToken({}).then(result => {
            const token = result.data.token
            let client = new OSS({
                region: this.region,
                accessKeyId: token.AccessKeyId,
                accessKeySecret: token.AccessKeySecret,
                stsToken: token.SecurityToken,
                bucket: token.Bucket
            });
            return client.multipartUpload(params.key, params.file, {}).then((res) => {
                if(res.url){
                    return resolve({
                        url: `${this.url}/${res.name}`,
                    })
                }else{
                    return reject(res)
                }
            }).catch((err) => {
                console.log(err)
            })
        })
    })
}
// class UploadFile extends Http {
//     constructor(){
//         super()
//         this.token = store.state.token
//         this.hostname = 'https://www.hanlinsys.com'
//         this.appServer = this.hostname + '/api/storage';
//         // this.bucket = bucketName || 'uploads';
//         this.region = 'oss-cn-beijing';
//         this.Url = ''
//         this.urllib = OSS.urllib;
//     }

//     applyTokenDo(func) {
//         var url = this.appServer;
//         console.log(StaticApi)
//         let StaticApiFn = new StaticApi()
//         StaticApiFn.getUploadToken({}).then(result => {
//             // var creds = JSON.parse(result.data);
//             const token = result.data.token
//             // this.Url = creds.Url
//             this.client = new OSS({
//                 region: this.region,
//                 accessKeyId: token.AccessKeyId,
//                 accessKeySecret: token.AccessKeySecret,
//                 stsToken: token.SecurityToken,
//                 bucket: token.Bucket
//             });
//             return func()
//         })
//         // this.urllib.request(url, {
//         //     method: 'GET',
//         //     headers: {
//         //         Authorization: `Bearer ${this.token}`
//         //     }
//         // }).then((result) => {
            
//         // });
//     }
//     uploadFile(params) {
//         return this.applyTokenDo(() => {
//             console.log(this.client.multipartUpload)
//             return this.client.multipartUpload(params.key, params.file).then((res) => {
//                 console.log('upload success: %j', res);
//                 return new Promise((resolve, reject) => {
//                     if(res.url){
//                         return resolve({
//                             url: `${this.url}/${res.name}`,
//                         })
//                     }else{
//                         return reject(res)
//                     }
//                 })
//             })
//         })
//     }
// }

// export default new UploadFile()