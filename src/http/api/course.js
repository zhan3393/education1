import Http from '../http';

export default class Course extends Http {
    add_course(params) {
        return this.instance.post(`/api/course/create`, params);
    }
    get_course_list(params) {
        return this.instance.get('/api/course', { params })
    }
    change_course(params, id) {
        return this.instance.post(`/api/course/${id}`, params)
    }
    get_uestionLabel(params) {
        return this.instance.get(`/api/questionLabel`, { params })
    }
    create_uestionLabel(params) {
        return this.instance.post(`/api/questionLabel/create`, params)
    }
    delete_uestionLabel(id, params) {
        return this.instance.post(`/api/questionLabel/delete/${id}`, params)
    }
    get_course_detail(id) {
        return this.instance.get(`/api/course/profile/${id}`)
    }
    get_course_mark_detail(id) {
        return this.instance.get(`/api/course/mark/${id}`)
    }
    get_teacher_list(params) {
        return this.instance.get(`/api/courseTeacher/list`, { params })
    }
    get_teacher_details(params) {
        return this.instance.get(`/api/courseTeacher/${params}`, {})
    }
    get_add_teacher(params) {
        return this.instance.get(`/api/courseTeacher`, { params })
    }
    add_teacher(params) {
        return this.instance.post(`/api/courseTeacher/create`, params)
    }
    add_student(params) {
        return this.instance.post(`/api/courseStudent/create`, params)
    }
    delete_teacher(params, id) {
        return this.instance.post(`/api/courseTeacher/delete/${id}`, params)
    }
    create_assignment(params) {
        return this.instance.post(`/api/assignment/create`, params)
    }
    update_assignment(params, id) {
        return this.instance.post(`/api/assignment/${id}`, params)
    }
    get_unit(params) {
        return this.instance.get(`/api/unit`, { params });
    }
    create_unit(params) {
        return this.instance.post(`/api/unit/create`, params);
    }
    edit_unit(id, params) {
        return this.instance.post(`/api/unit/${id}`, params);
    }
    del_unit(id, params) {
        return this.instance.post(`/api/unit/delete/${id}`, params);
    }

    publish_assignment(params) {
        return this.instance.post(`/api/assignment/publish/${params}`, {})
    }
    assignment_details(params) {
        return this.instance.get(`/api/assignment/${params}`, {})
    }
    get_courseStudent_list(params) {
        return this.instance.get(`/api/courseStudent/list`, { params })
    }
    get_courseStudent(params) {
        return this.instance.get(`/api/courseStudent`, { params })
    }
    get_StudentCurse(params) {
        return this.instance.get(`/api/courseStudent/course`, { params })
    }
    get_courseClass_list(params) {
        return this.instance.get(`/api/courseClass/list`, { params })
    }
    get_courseAssistant_list(params) {
        return this.instance.get(`/api/courseAssistant/list`, { params })
    }
    get_courseAssistant(params) {
        return this.instance.get(`/api/courseAssistant`, { params })
    }
    get_courseClass(params) {
        return this.instance.get(`/api/courseClass`, { params })
    }
    create_courseClass(params) {
        return this.instance.post(`/api/courseClass/create`, params)
    }
    create_courseClass_temp(params) {
        return this.instance.post(`/api/courseClass/createTemp`, params)
    }
    create_courseAssistant(params) {
        return this.instance.post(`/api/courseAssistant/create`, params)
    }
    delete_courseAssistant(params, id) {
        return this.instance.post(`/api/courseAssistant/delete/${id}`, params)
    }
    delete_courseClass(params, id) {
        return this.instance.post(`/api/courseClass/delete/${id}`, params)
    }
    updata_courseClass(params, id) {
        return this.instance.post(`/api/courseClass/${id}`, params)
    }
    student_courseClass(params) {
        return this.instance.get(`/api/courseClass`, { params })
    }
    delete_courseStudent(params) {
        return this.instance.post(`/api/courseStudent/delete/${params}`, {})
    }
    copy_assignment(id) {
        return this.instance.post(`/api/assignment/copy/${id}`, {})
    }
    get_course_cno(cno) {
        return this.instance.get(`/api/course/cno/${cno}`, {})
    }
    get_course_invite_code(inviteCode) {
        return this.instance.get(`/api/courseClass/inviteCode/${inviteCode}`, {})
    }
    delete_course(id) {
        return this.instance.post(`/api/course/delete/${id}`, {})
    }
    course_todo(id) {
        return this.instance.get(`/api/course/todo/${id}`, {})
    }
    course_speed(id) {
        return this.instance.get(`/api/course/classes/${id}`, {})
    }
    get_courseClass_summary(id) {
        return this.instance.get(`/api/courseClass/summary/${id}`, {})
    }
    get_course_category(id, params) {
        return this.instance.post(`/api/course/category/${id}`, params)
    }
    get_course_present(id) {
        return this.instance.get(`/api/courseClass/present/${id}`, {})
    }
    set_course_present(id, params) {
        return this.instance.post(`/api/courseClass/present/${id}`, params)
    }
    task_report(id, params) {
        return this.instance.get(`/api/courseClass/taskReport/${id}`, { params })
    }
    updata_task_report(id, params) {
        return this.instance.post(`/api/courseClass/taskCategoryRatio/${id}`, params)
    }
    courseClass_addTeacher(id, params){
        return this.instance.post(`/api/courseClass/addTeacher/${id}`, params)
    }
    courseClass_addAssistant(id, params){
        return this.instance.post(`/api/courseClass/addAssistant/${id}`, params)
    }
    courseClass_deleteTeacher(id, params){
        return this.instance.post(`/api/courseClass/deleteTeacher/${id}`, params)
    }
    courseClass_deleteAssistant(id, params){
        return this.instance.post(`/api/courseClass/deleteAssistant/${id}`, params)
    }
}
