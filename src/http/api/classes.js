import Http from '../http';

export default class Classes extends Http{
    get_class() {
        return this.instance.get(`/api/classes`);
    }
    get_class_lates_report(id) {
        return this.instance.get(`/api/classes/latestReport/${id}`)
    }
    create_class(params) {
        return this.instance.post(`/api/classes/create`, params);
    }
    edit_class(id, params) {
        return this.instance.post(`/api/classes/${id}`, params);
    }
    del_class(id, params) {
        return this.instance.post(`/api/classes/delete/${id}`, params);
    }
    get_class_student(params) {
        return this.instance.get(`/api/classStudent`, {params})
    }
    add_class_student(params) {
        return this.instance.post(`/api/classStudent/create`, params)
    }
    remove_class_student(id, params) {
        return this.instance.post(`/api/classStudent/delete/${id}`, params);
    }
    get_class_course(params) {
        return this.instance.get(`/api/classCourse`, {params})
    }
    create_class_course(params) {
        return this.instance.post(`/api/classCourse/create`, params)
    }
    get_class_course_detail(id) {
        return this.instance.get(`/api/classCourse/${id}`, {})
    }
    get_class_exams(params) {
        return this.instance.get("/api/classes/exams", params)
    }
    get_class_report(params) {
        return this.instance.get("/api/classReport", {params})
    }
    get_class_report_detail(id) {
        return this.instance.get(`/api/classReport/preview/${id}`, {})
    }
    create_class_report(params) {
        return this.instance.post(`/api/classReport/create`, params)
    }
    update_class_report(id, params) {
        return this.instance.post(`/api/classReport/${id}`, params)
    }
    remove_class_report(id, params) {
        return this.instance.post(`/api/classReport/delete/${id}`, params)
    }
    get_class_unfinished_assignment(id) {
        return this.instance.get(`/api/classes/unfinishedAssignment/${id}`, {})
    }
    get_class_unfinished_exam(id) {
        return this.instance.get(`/api/classes/unfinishedExam/${id}`, {})
    }
    get_class_progress(id) {
        return this.instance.get(`/api/classes/progress/${id}`, {})
    }
} 