import Http from '../http';

export default class Lab extends Http{
    questionBank(params){
        return this.instance.get(`/api/questionBank`, {params})
    }
    questionBankDetails(params){
        return this.instance.get(`/api/questionBank/${params}`, {})
    }
    questionBankAnalysis(id, params){
        return this.instance.post(`/api/questionBank/analysis/${id}`, params)
    }
    questionBankLike(id){
        return this.instance.post(`/api/questionBank/like/${id}`, {})
    }
    questionBankLabel(id, params){
        return this.instance.post(`/api/questionBank/label/${id}`, params)
    }
    questionUploadCreate(params){
        return this.instance.post(`/api/questionUpload/create`, params)
    }
}