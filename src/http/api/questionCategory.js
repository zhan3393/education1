import Http from '../http';

export default class QuestionCategory extends Http {
    query_list(){
        return this.instance.get(`/api/questionCategory/tree`);
    }
}
