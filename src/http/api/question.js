import Http from '../http';

export default class Question extends Http{
    create_question(params){
        return this.instance.post(`/api/question/createMulti`, params);
    }
    create_question_item(params){
        return this.instance.post(`/api/question/create`, params);
    }
    update_questions(params){
        return this.instance.post(`/api/question/updateMulti`, params)
    }
    update_question_item(params, id){
        return this.instance.post(`/api/question/${id}`, params)
    }
    delete_question(id){
        return this.instance.post(`/api/question/delete/${id}`, {})
    }
    delete_question_no_model(id){
        return this.instance.post(`/api/question/delete/${id}`, {refresh: true})
    }
    scanningLocal(params){
        return this.instance.post(`/api/exam/scan`, params)
    }
}