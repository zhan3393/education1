import Http from '../http';

export default class Report extends Http{
    generate_assignment_report(id, params){
        return this.instance.post(`/api/stuAssignment/scoreClass/${id}`, params)
    }
    get_assignment_report_list(id, params){
        return this.instance.get(`/api/stuAssignment/report/list/${id}`, {params})
    }
    get_assignment_report(id, params){
        return this.instance.get(`/api/stuAssignment/report/${id}`, {params})
    }
    get_assignment_dispersion(id, params){
        return this.instance.post(`/api/stuAssignment/dispersionChart/${id}`, params)
    }
    update_assignment_ratio(id, params){
        return this.instance.post(`/api/stuAssignment/updateRatio/${id}`, params)
    }
    generate_exam_report(id, params){
        return this.instance.post(`/api/stuExam/scoreClass/${id}`, params)
    }
    get_exam_report_list(id, params) {
        return this.instance.get(`/api/stuExam/report/list/${id}`, {params})
    }
    get_exam_report(id, params){
        return this.instance.get(`/api/stuExam/report/${id}`, {params})
    }
    get_exam_dispersion(id, params){
        return this.instance.post(`/api/stuExam/dispersionChart/${id}`, params)
    }
    update_exam_ratio(id, params){
        return this.instance.post(`/api/stuExam/updateRatio/${id}`, params)
    }
    get_stu_assignment_report(id) {
        return this.instance.get(`/api/stuAssignment/assignmentReport/${id}`, {})
    }
    get_stu_exam_report(id) {
        return this.instance.get(`/api/stuExam/examReport/${id}`, {})
    }
}