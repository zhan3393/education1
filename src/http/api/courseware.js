import Http from '../http'

export default class Courseware extends Http {
    create_courseware(params){
        return this.instance.post('/api/courseware/create', params)
    }
    edit_courseware(id, params){
        return this.instance.post(`/api/courseware/${id}`, params)
    }
    del_courseware(id, params){
        return this.instance.post(`/api/courseware/delete/${id}`, params)
    }
    edit_courseware_visible(id, params) {
        return this.instance.post(`/api/courseware/visible/${id}`, params)
    }
    courseware_details(id){
        return this.instance.get(`/api/courseware/${id}`, {})
    }
    publish_courseware(id){
        return this.instance.post(`/api/courseware/publish/${id}`, {})
    }
    copy_courseware(id){
        return this.instance.post(`/api/courseware/copy/${id}`, {})
    }
}