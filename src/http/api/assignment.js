import Http from '../http';

export default class Assignment extends Http{
    get_assignment(params) {
        return this.instance.get(`/api/assignment`, {params});
    }
    create_assignment(params) {
        return this.instance.post(`/api/assignment/create`, params);
    }
    edit_assignment(id, params) {
        return this.instance.post(`/api/assignment/${id}`, params);
    }
    del_assignment(id, params) {
        return this.instance.post(`/api/assignment/delete/${id}`, params);
    }
    edit_assignment_visible(id, params) {
        return this.instance.post(`/api/assignment/visible/${id}`, params);
    }
    get_unfinished_student(id, params){
        return this.instance.get(`/api/stuAssignment/classUnfinished/${id}`, {params});
    }
    get_multiClassChart(params){
        return this.instance.post(`/api/stuAssignment/multiClassChart`, params)
    }
    getAttachment(id, _id){
        return this.instance.get(`/api/stuAssignment/attachment/${id}?_id=${_id}`, {})
    }
    saveAttachment(id, params){
        return this.instance.post(`/api/stuAssignment/attachment/${id}`, params)
    }
}