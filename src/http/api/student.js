import Http from '../http';

export default class Student extends Http{
    student_current(params){
        return this.instance.get(`/api/student/current`, {params})
    }
    get_student(params, hide){
        return this.instance.get(`/api/student`, {params}, hide);
    }
    get_student_assignment(params){
        return this.instance.get(`/api/stuAssignment/student`, {params})
    }
    get_student_assignment_detail(id){
        return this.instance.get(`/api/stuAssignment/${id}`, {})
    }
    get_student_course_notice(params){
        return this.instance.get(`/api/courseNotice/receiver`, {params})
    }
    get_student_class_notice(params){
        return this.instance.get(`/api/classNotice/receiver`, {params})
    }
    submit_student_assignment(id, params){
        return this.instance.post(`/api/stuAssignment/submit/${id}`, params)
    }
    get_student_course(params){
        return this.instance.get(`/api/courseStudent/student`, {params})
    }
    get_student_course_detail(id, params){
        return this.instance.get(`/api/courseStudent/profile/${id}`, {params})
    }
    student_join_course(params){
        return this.instance.post(`/api/courseStudent/join`, params)
    }
    get_student_courseware_detail(id){
        return this.instance.get(`/api/stuCourseware/${id}`, {})
    }
    get_student_calendar(params){
        return this.instance.post(`/api/student/calendar`, params)
    }
    student_courseReport(id, params){
        return this.instance.post(`/api/student/courseReport/${id}`, params || {})
    }
    delete_course_notice(id){
        return this.instance.post(`/api/courseNotice/delete/${id}`, {})
    }
    delete_class_notice(id){
        return this.instance.post(`/api/classNotice/delete/${id}`, {})
    }
}