import Http from '../http'

export default class StaticApi extends Http{
    getUploadToken(params){
        return this.instance.get(`/api/storage`, params);
    }
}