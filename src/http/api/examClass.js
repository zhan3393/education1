import Http from '../http';

export default class ExamClass extends Http{
    getExamClass(id) {
        return this.instance.get(`/api/examClass/exam/` + id, {});
    }
    examClassCreate(params){
        return this.instance.post('/api/examClass/create', params)
    }
    updateExamClass(id, params){
        return this.instance.post('/api/examClass/' + id, params)
    }
    deleteExamClass(id){
        return this.instance.post('/api/examClass/delete/' + id, {})
    }
    unfreezeExamClass(id){
        return this.instance.post('/api/examClass/unfreeze/' + id, {})
    }
    getAssignmentsClass(id) {
        return this.instance.get(`/api/assignmentClass/assignment/` + id, {});
    }
    assignmentsClassCreate(params){
        return this.instance.post('/api/assignmentClass/create', params)
    }
    updateAssignmentsClass(id, params){
        return this.instance.post('/api/assignmentClass/' + id, params)
    }
    deleteAssignmentsClass(id){
        return this.instance.post('/api/assignmentClass/delete/' + id, {})
    }
    unfreezeAssignmentsClass(id){
        return this.instance.post('/api/assignmentClass/unfreeze/' + id, {})
    }
    getCoursewaresClass(id) {
        return this.instance.get(`/api/coursewareClass/courseware/` + id, {});
    }
    coursewaresClassCreate(params){
        return this.instance.post('/api/coursewareClass/create', params)
    }
    updateCoursewaresClass(id, params){
        return this.instance.post('/api/coursewareClass/' + id, params)
    }
    deleteCoursewaresClass(id){
        return this.instance.post('/api/coursewareClass/delete/' + id, {})
    }
    unfreezeCoursewaresClass(id){
        return this.instance.post('/api/coursewareClass/freeze/' + id, {})
    }

    reset_password_student(params) {
        return this.instance.post(`/api/student/resetPsw`, params);
    }
    update_user_student(params){
        return this.instance.post('/api/student/resetSelf', params)
    }
}