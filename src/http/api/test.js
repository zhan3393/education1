import Http from '../http'

export default class Test extends Http {
    create_test(params) {
        return this.instance.post(`/api/test/create`, params)
    }
    edit_test(id, params) {
        return this.instance.post(`/api/test/${id}`, params)
    }
    del_test(id, params) {
        return this.instance.post(`/api/test/delete/${id}`, params)
    }
    query_test(id) {
        return this.instance.get(`/api/test/${id}`)
    }
    scan_test(id, params) {
        return this.instance.post(`/api/test/scan/${id}`, params)
    }
    set_test_performance(id, params) {
        return this.instance.post(`/api/test/performance/${id}`, params)
    }

    // 随堂测试问题
    get_test_questions(params) {
        return this.instance.get(`/api/testQuestion`, { params })
    }
    create_test_question(params) {
        return this.instance.post(`/api/testQuestion/create`, params)
    }
    edit_test_question(id, params) {
        return this.instance.post(`/api/testQuestion/${id}`, params)
    }
    del_test_question(id) {
        return this.instance.post(`/api/testQuestion/delete/${id}`)
    }

    // 随堂测试班级
    create_test_class(params) {
        return this.instance.post(`/api/testClass/create`, params)
    }
    query_test_classes(params) {
        return this.instance.get(`/api/testClass`, { params })
    }
    query_test_class(id) {
        return this.instance.get(`/api/testClass/${id}`)
    }
    start_present(id) {
        return this.instance.post(`/api/testClass/startPresent/${id}`)
    }
    close_present(id) {
        return this.instance.post(`/api/testClass/closePresent/${id}`)
    }
    start_question(id, params) {
        return this.instance.post(`/api/testClass/startQuestion/${id}`, params)
    }
    close_question(id, params) {
        return this.instance.post(`/api/testClass/closeQuestion/${id}`, params)
    }

    // 学生成绩
    query_stutest(params) {
        return this.instance.get(`/api/stuTest/list`, { params })
    }
}
