import Http from '../http';

export default class Message extends Http{
    get_message(params) {
        return this.instance.get(`/api/message/sender`, {params});
    }
    get_teacher_notice(params){
        return this.instance.get(`/api/notice`, {params})
    }
    delete_teacher_notice(params){
        return this.instance.post(`/api/notice/deleteMulti`, params)
    }
    readAll_teacher_notice(){
        return this.instance.post(`/api/notice/readAll`, {})
    }
    readMulti_teacher_notice(params){
        return this.instance.post(`/api/notice/readMulti`, params)
    }
    get_notice(params){
        return this.instance.get(`/api/notice`, {params})
    }
    sender_courseNotice(params){
        return this.instance.get(`/api/courseNotice/sender`, {params})
    }
    get_courseNotice(params){
        return this.instance.get(`/api/courseNotice/receiver`, {params})
    }
    create_courseNotice(params){
        return this.instance.post(`/api/courseNotice/send`, params)
    }
    sender_class_notice(params) {
        return this.instance.get(`/api/classNotice/sender`, {params})
    }
    get_class_notice(params) {
        return this.instance.get(`/api/classNotice/receiver`, {params})
    }
    // get_class_notice(id) {
    //     return this.instance.get(`/api/classNotice/${id}`)
    // }
    create_class_notice(params) {
        return this.instance.post(`/api/classNotice/send`, params)
    }
    edit_class_notice(id, params) {
        return this.instance.post(`/api/classNotice/${id}`, params)
    }
    del_class_notice(id, params) {
        return this.instance.post(`/api/classNotice/delete/${id}`, params)
    }
    send_to_student(params){
        return this.instance.post(`/api/message/toStudent`, params)
    }
    send_to_message(params){
        return this.instance.post(`/api/message/send`, params)
    }
    updata_notice(id){
        return this.instance.post(`/api/message/read/${id}`, {})
    }
    get_PrivateLetter(params){
        return this.instance.get(`/api/message/receiver`, {params})
    }
    delete_message(params){
        return this.instance.post(`/api/message/deleteMulti`, params)
    }
    readMulti_message(params){
        return this.instance.post(`/api/message/readMulti`, params)
    }
    readAll_message(){
        return this.instance.post(`/api/message/readAll`, {})
    }
    delete_class_notice_multi(params){
        return this.instance.post(`/api/classNotice/deleteMulti`, params)
    }
    ready_class_notice_multi(params){
        return this.instance.post(`/api/classNotice/readMulti`, params)
    }
    ready_class_notice_all(){
        return this.instance.post(`/api/classNotice/readAll`, {})
    }
    delete_course_notice_multi(params){
        return this.instance.post(`/api/courseNotice/deleteMulti`, params)
    }
    ready_course_notice_multi(params){
        return this.instance.post(`/api/courseNotice/readMulti`, params)
    }
    ready_course_notice_all(){
        return this.instance.post(`/api/courseNotice/readAll`, {})
    }
    get_unread_class_notice() {
        return this.instance.get(`/api/classNotice/unread`, {})
    }
    get_unread_course_notice() {
        return this.instance.get(`/api/courseNotice/unread`, {})
    }
    get_unread_message_notice() {
        return this.instance.get(`/api/message/unread`, {})
    }
}