import Http from '../http'

export default class Event extends Http {
    create_event(params){
        return this.instance.post(`/api/event/create`, params)
    }
    get_event(params){
        return this.instance.get(`/api/event`, {params})
    }
    del_event(id, params){
        return this.instance.post(`/api/event/delete/${id}`, params)
    }
}