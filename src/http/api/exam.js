import Http from '../http'

export default class Exam extends Http {
    create_exam(params){
        return this.instance.post('/api/exam/create', params)
    }
    edit_exam(id, params){
        return this.instance.post(`/api/exam/${id}`, params || {})
    }
    del_exam(id, params){
        return this.instance.post(`/api/exam/delete/${id}`, params)
    }
    get_exam_details(params) {
        return this.instance.get(`/api/exam/${params}`, {});
    }
    query_exam(params){
        return this.instance.get(`/api/exam`, {params});
    }
    exam_pubilsh(id){
        return this.instance.post(`/api/exam/publish/${id}`, {})
    }
    copy_exam(id){
        return this.instance.post(`/api/exam/copy/${id}`, {})
    }
    edit_exam_visible(id, params) {
        return this.instance.post(`/api/exam/visible/${id}`, params);
    }
    get_edu_classQustion(id, params){
        return this.instance.post(`/api/stuExam/classQuestion/${id}`, params);
    }
    stu_answer_question(id, params){
        return this.instance.post(`/api/stuExam/answer/${id}`, params);
    }
    stu_submit_exam(id){
        return this.instance.post(`/api/stuExam/submit/${id}`, {});
    }
    stu_exam_details(id){
        return this.instance.get(`/api/stuExam/${id}`, {});
    }
    start_exam(id){
        return this.instance.post(`/api/stuExam/start/${id}`, {});
    }
    teacher_check_exam(id, params){
        return this.instance.post(`/api/stuExam/check/${id}`, params);
    }
    student_check_exam(params){
        return this.instance.get(`/api/stuExam/student`, {params});
    }
    teacher_get_read_over(id, params){
        return this.instance.post(`/api/stuExam/examClass/${id}`, params);
    }
    stuExam_scoreClass(id, params){
        return this.instance.post(`/api/stuExam/classExam/${id}`, params);
    }
    get_unfinished_student(id, params){
        return this.instance.get(`/api/stuExam/classUnfinished/${id}`, {params});
    }
    get_classLatestChart(id){
        return this.instance.post(`/api/stuExam/classLatestChart/${id}`, {});
    }
    student_reviewExam(id){
        return this.instance.get(`/api/student/reviewExam/${id}`, {})
    }
    get_multiClassChart(params){
        return this.instance.post(`/api/stuExam/multiClassChart`, params)
    }
    start_exams(id, params){
        return this.instance.post(`/api/examClass/startExam/${id}`, params)
    }
}