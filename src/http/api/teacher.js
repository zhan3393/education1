import Http from '../http';

export default class Teacher extends Http{
    teacher_login(params){
        return this.instance.post(`/teacher/login`, params);
    }
    teacher_current(params){
        return this.instance.get(`/api/teacher/current`, {params});
    }
    get_student_assignment(params){
        return this.instance.get(`/api/stuAssignment`, {params})
    }
    get_student_assignment_summary(params){
        return this.instance.get(`/api/stuAssignment/summary`, {params})
    }
    teacher_get_stuAssignment(id){
        return this.instance.post(`/api/stuAssignment/check/${id}`, {})
    }
    get_stuAssignment_details(id){
        return this.instance.get(`/api/stuAssignment/${id}`, {})
    }
    stuAssignment_updata(id, params){
        return this.instance.post(`/api/stuAssignment/check/${id}`, params)
    }
    get_stuExam(params){
        return this.instance.get(`/api/stuExam`, {params})
    }
    get_stuExam_summery(params){
        return this.instance.get(`/api/stuExam/summary`, {params})
    }
    update_stuAssignment(id, params){
        return this.instance.post(`/api/stuAssignment/update/${id}`, params)
    }
    update_stuExam(id, params){
        return this.instance.post(`/api/stuExam/${id}`, params)
    }
    get_teacher_calendar(params){
        return this.instance.post(`/api/teacher/calendar`, params)
    }
    change_format_pdf(params){
        return this.instance.post(`/api/course/scanOffice`, params)
    }
}