import Http from '../http';

export default class Account extends Http{
    reset_password(params) {
        return this.instance.post(`/api/teacher/resetPsw`, params);
    }
    update_user(params){
        return this.instance.post('/api/teacher/resetSelf', params)
    }
    reset_password_student(params) {
        return this.instance.post(`/api/student/resetPsw`, params);
    }
    update_user_student(params){
        return this.instance.post('/api/student/resetSelf', params)
    }
}