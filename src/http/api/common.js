import Http from '../http';

export default class Common extends Http{
    get_teacher_list(params) {
        return this.instance.get(`/api/teacher`, {params});
    }
    registerTeacher(params){
        return this.instance.post(`/teacher/register`, params)
    }
    registerStudent(params){
        return this.instance.post(`/student/register`, params)
    }
    studentLogin(params){
        return this.instance.post(`/student/login`, params)
    }
    get_student_list(params){
        return this.instance.get(`/api/student`, {params})
    }
    message_unread(){
        return this.instance.get(`/api/unread`, {})
    }
    taskCategory(id){
        let params = {
            current: 1,
            pageSize: 999999,
            where: JSON.stringify({
                course: id
            })
        }
        return this.instance.get(`/api/taskCategory`, {params})
    }
    addTaskCategory(params){
        return this.instance.post(`/api/taskCategory/create`, params)
    }
    delTaskCategory(id, params){
        return this.instance.post(`/api/taskCategory/delete/${id}`, params)
    }
}