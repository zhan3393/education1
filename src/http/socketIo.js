import io from 'socket.io-client'
import Cookie from 'js-cookie'

export default function(params = {}) {
  const token = Cookie.get('edu_token') || ''

  if (!token) {
    return false
  }

  const { url = 'ws.hanlinsys.com' } = params
  const uri = `https://${url}`
  const query = params.query || {
    course: params.course || '', //course id
    type: params.type || '', // exam courseware assignment  三种类型 （在列表页时为空字符串）
    type_id: params.type_id || '', // type的主键id 例如 exam的主键id （在列表页时为空字符串）
  }

  const socket = io(uri, {
    transports: ['websocket', 'polling'],
    upgrade: false,
    query: { ...query, token },
  })

  socket.on('connect', () => {
    console.log('连接成功')
  })
  socket.emit('course', 'xixihaha')

  return socket
}
