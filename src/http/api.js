import Teacher from './api/teacher'
import Common from './api/common'
import Course from './api/course'
import Test from './api/test'
import Question from './api/question'
import QuestionCategory from './api/questionCategory'
import Assignment from './api/assignment'
import Exam from './api/exam'
import Account from './api/account'
import Message from './api/message'
import Classes from './api/classes'
import Student from './api/student'
import Correct from './api/correct'
import Courseware from './api/courseware'
import Event from './api/event'
import Report from './api/report'
import Lab from './api/lab'
import ExamClass from './api/ExamClass'

export default {
    Teacher: new Teacher(),
    Common: new Common(),
    Course: new Course(),
    Question: new Question(),
    QuestionCategory: new QuestionCategory(),
    Assignment: new Assignment(),
    Exam: new Exam(),
    Account: new Account(),
    Message: new Message(),
    Classes: new Classes(),
    Student: new Student(),
    Correct: new Correct(),
    Courseware: new Courseware(),
    Event: new Event(),
    Report: new Report(),
    Lab: new Lab(),
    Test: new Test(),
    ExamClass: new ExamClass()
}
