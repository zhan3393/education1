`/api/teacher`      //查询教师
`/api/student`      //查询学生

'/api/course'                 //查询课程
`/api/courseClass`            //查询课程班级
`/api/courseClass/list`       //查询课程班级及关联
`/api/courseStudent`          //查询课程学生
`/api/courseStudent/course`   //
`/api/courseStudent/student`  //学生查询课程
`/api/courseAssistant`        //查询课程助教

`/api/classCourse`   //查询班级课程
`/api/classStudent`  //查询班级学生
`/api/classes/unfinishedAssignment/${id}`  //班级未完成作业
`/api/classes/unfinishedExam/${id}`        //班级未完成测试

`/api/taskCategory`   //查询任务分类
`/api/questionLabel`  //


`/api/stuAssignment/report/list/${id}`     //查询作业报告
`/api/stuExam/report/list/${id}`           //查询考试报告列表
`/api/stuAssignment/classUnfinished/${id}` //班级最近未提交作业学员
`/api/stuExam/classUnfinished/${id}`       //班级最近未提交考试学员