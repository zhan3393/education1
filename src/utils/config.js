export default {
  hostName: {
    development: "/proxys",
    production: "",
    debug: ""
  },
  interfaceBase: "",
  labelColor: {
    default: '#A21832',
    type1: '#A21832',
    type2: '#E98E1D',
    type3: '#FFCB00',
    type4: '#A1C618',
    type5: '#23AC8A',
    type6: '#269DC2',
    type7: '#3E96FF',
    type8: '#3471BA',
    type9: '#2929DD',
    type10: '#C529DD',
    type11: '#DD2987',
    type12: '#DD2952',
  },
  labelColors: ['#A21832', '#E98E1D', '#FFCB00', '#A1C618', '#23AC8A', '#269DC2', '#3E96FF', '#3471BA', '#2929DD', '#C529DD', '#DD2987', '#DD2952'],
  letter: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
  course: {
    events: "rgba(52, 113, 186, 0.3)",
    exams: "rgb(246, 158, 0, 0.3)",
    assignments: "rgb(24, 162, 148, 0.3)",
    coursewares: "rgb(162, 24, 50, 0.3)",
  },
  layout: {
    image: ['gif', 'jpeg', 'jpg', 'png', 'bmp', 'tif'],
    word: ['doc', 'docx', 'dotx', 'dotm', 'docm'],
    mp3: ['mp3'],
    mp4: ['mp4', 'avi', 'wmv', 'mpeg', 'm4v', 'rm'],
    excel: ['xls', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xlam'],
    powerPoint: ['ppt', 'pptx', 'pptm', 'ppsx', 'ppsm', 'potx', 'potm', 'ppam'],
    other: ['pdf']
  },
  accept: {
    image: "image/gif, image/jpeg, image/x-png",
    mp3: "audio/mp3",
    mp4: "video/mp4,audio/m4a",
    office: 'application/msexcel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,'
  }
}
