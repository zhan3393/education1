class Utils{
  // 获取url参数
  getQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(decodeURI(unescape(r[2])));
    return null;
  }
  // 获取字符串的长度，包含汉字等
  getStringLength(s){
    let reg = /[^\x00-\xff]/g
    s = String(s)
    let length = s.length
    let chinaLength = (s.match(reg) || "").length
    return (length - chinaLength) + chinaLength * 2
  }
  formatDate(date, fmt) {
    // "yyyy-MM-dd hh:mm:ss"
    fmt = fmt || "yyyy-MM-dd hh:mm:ss"
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    };
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        let str = o[k] + '';
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : this.padLeftZero(str));
      }
    }
    return fmt;
  }
  getNowTime(){
    let newTime = new Date()
    return this.formatDate(newTime, "yyyy-MM-dd hh:mm:ss")
  }
  getDate(month, date){
    let nowTime = new Date()
    nowTime.setMonth(nowTime.getMonth() + month)
    if(date){
      nowTime.setDate(date)
    }
    return this.formatDate(nowTime, "yyyy-MM-dd hh:mm:ss")
  }
  padLeftZero(str){
    return ('00' + str).substr(str.length);
  }
  checkCode(code){
    return /^\d{6}$/.test(code)
  }
  changeTime(date, time){
    if(!date){
      return false
    }
    time = time || '00:00'
    return new Date(`${date} ${time}:00`).getTime()
  }
  getMin(time){
    if(!time){
      return ''
    }
    return 60 * Number(time.split(":")[0]) + Number(time.split(":")[1])
  }
  formatMin(min){
    if(!min){
      return `00:00`
    }
    return (min/60>>0) + ":" + (min%60)
  }
  formatStr(str){
    return encodeURI(encodeURI(str))
  }
  randomString(len) {
  　　len = len || 32;
  　　var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
  　　var maxPos = $chars.length;
  　　var pwd = '';
  　　for (let i = 0; i < len; i++) {
  　　　　pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
  　　}
  　　return pwd;
  }
  getSuffix(url){
    return url.substr(url.lastIndexOf('.') + 1).toLowerCase()
  }
  getFloat(number, n){
    n = n ? parseInt(n) : 0;
    if(n <= 0) {
      return Math.round(number);
    }
    number = Math.round(number * Math.pow(10, n)) / Math.pow(10, n); //四舍五入
    number = Number(number).toFixed(n); //补足位数
    return number;
  }
}
export default new Utils()
