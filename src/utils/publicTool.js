class PublicTool {
  constructor() {
    this.questionList = {
      Single: {
        type: {
          type: 'Single',
          name: '单选题',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          type: 'Single',
          points: 0,
          answer: '',
          previewAnswer: '',
          label: '',
          options: [
            {
              key: "A",
              content: ''
            },
            {
              key: "B",
              content: ''
            },
            {
              key: "C",
              content: ''
            },
            {
              key: "D",
              content: ''
            },
          ],
          images: [],
          analysis: ''
        }
      },
      Multiple: {
        type: {
          type: 'Multiple',
          name: '多选题',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          type: 'Multiple',
          points: 0,
          answer: [],
          previewAnswer: [],
          label: '',
          options: [
            {
              key: "A",
              content: ''
            },
            {
              key: "B",
              content: ''
            },
            {
              key: "C",
              content: ''
            },
            {
              key: "D",
              content: ''
            },
          ],
          images: [],
          analysis: ''
        }
      },
      Blanks: {
        type: {
          type: 'Blanks',
          name: '填空题',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          type: 'Blanks',
          points: 0,
          label: '',
          answer: '',
          images: [],
          analysis: ''
        }
      },
      Text: {
        type: {
          type: 'Text',
          name: '简答题',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          type: 'Text',
          points: 0,
          label: '',
          answer: '',
          images: [],
          analysis: ''
        }
      },
      Page: {
        type: {
          type: 'Page',
          name: '分页',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          label: '',
          type: 'Page',
          answer: '',
          images: []
        }
      },
      Reading: {
        type: {
          type: 'Reading',
          name: '段落说明',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          type: 'Reading',
          points: 0,
          answer: '',
          images: [],
          label: '',
        }
      },
      Composition: {
        type: {
          type: 'Composition',
          name: '作文题',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          type: 'Composition',
          points: 0,
          label: '',
          answer: '',
          images: [],
          analysis: ''
        }
      },
      Upload: {
        type: {
          type: 'Upload',
          name: '上传题',
          icon: 'el-icon-circle-plus'
        },
        content: {
          content: '',
          type: 'Upload',
          points: 0,
          label: '',
          answer: '',
          images: [],
          analysis: ''
        }
      },
    }
  }

  selectQuestionType(type, count) {
    if (!type || !count) {
      return false
    }
    let list = []
    for (let i = 0; i < Number(count); i++) {
      list.push(this.questionList[type])
    }
    return list
  }

  // 汉字排序
  pySegSort(list) {
    return list.sort((a, b) => {
      return a.localeCompare(b);
    })
  }
}

export default new PublicTool()
