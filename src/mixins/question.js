import config from '../utils/config'
import EventBus from '../utils/eventVue'
import draggable from 'vuedraggable'
import utils from '../utils/index'
import PublicTool from '../utils/publicTool'

export default {
  components: {
    draggable
  },
  props: ["labelDefaultIndex", "labelListAll"],
  data() {
    return {
      labelColor: config.labelColors,
      QuestionTypeColor: config.labelColors[this.labelDefaultIndex],
      letter: config.letter,
      dialogVisible: false,
      oldIndex: 0,
      groupCover: {
        name: 'cover'
      },
      labelList: [],
      editFlag: true,
      exam: {},
      typeList: [
        {
          type: 'Single',
          name: '单选题',
          icon: 'el-icon-circle-plus'
        },
        {
          type: 'Multiple',
          name: '多选题',
          icon: 'el-icon-circle-plus'
        },
        {
          type: 'Blanks',
          name: '填空题',
          icon: 'el-icon-circle-plus'
        },
        {
          type: 'Text',
          name: '简答题',
          icon: 'el-icon-circle-plus'
        },
        {
          type: 'Reading',
          name: '段落说明',
          icon: 'el-icon-circle-plus'
        },
        {
          type: 'Composition',
          name: '作文题',
          icon: 'el-icon-circle-plus'
        },
        {
          type: 'Upload',
          name: '上传题',
          icon: 'el-icon-circle-plus'
        },
      ],
      newType: '',
      questionNo: '',
      accept: config.accept.image,
      layout: config.layout.image,
    }
  },
  methods: {
    deleteQuestionOption(index) {
      this.innerItem.content.options.splice(index, 1)
      if (this.innerItem.content.type == "Single") {
        this.innerItem.content.answer = ''
      } else {
        this.innerItem.content.answer = []
      }
      this.formatOption()
    },
    formatOption() {
      this.innerItem.content.options.map((item, index) => {
        item.key = this.letter[index]
      })
    },
    addLabelItem(e) {
      if (e.added) {
        this.labelList = [e.added.element]
        this.innerItem.content.label = e.added.element

        this.$nextTick(() => {
          let labelIndex = this.labelListAll.indexOf(this.innerItem.content.label)
          this.QuestionTypeColor = labelIndex >= 0 ? this.labelColor[labelIndex] : ''
        })
      }
    },
    changeType(val) {
      if (val) {
        this.$emit("changeQuestionItemType", {val: val, index: this.indexItem - 1})
      }
    },
    addTrueAnswer() {
      this.dialogVisible = true
    },
    closeDialog() {
      this.dialogVisible = false
      // this.innerItem.content.answer = ''
    },
    deleteQuestion() {
      this.$confirm('此操作将永久删除该题目, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        if (this.innerItem.content._id) {
          this.sendDeleteQusetion(this.innerItem.content._id, () => {
            this.$emit("deleteQuestion", this.indexItem - 1)
          })
        } else {
          this.$message.success("删除成功")
          this.$emit("deleteQuestion", this.indexItem - 1)
        }
      }).catch()
    },
    sendDeleteQusetion(id, fn) {
      this.$api.Question.delete_question(id).then(res => {
        if (res.code == 200) {
          this.$message.success("删除成功")
          fn && fn()
        } else {
          this.$message.error(res.error || "删除失败")
        }
      })
    },
    clickEdit() {
      this.editFlag = !this.editFlag
      if (!this.editFlag) {
        const params = JSON.parse(JSON.stringify(this.innerItem.content))
        if (params._id) {
          this.$api.Question.update_question_item(params, params._id).then(res => {
            console.log(res)
          })
        } else {
          params.exam = this.exam.exam
          this.$api.Question.create_question_item(params).then(res => {
            if (res.code == 200) {
              console.log(res)
            }
          })
        }
      }

      this.$emit("clickEdit", this.indexItem - 1)
    },
    changeQuestionType(newVal) {
      this.newType = newVal.content.type
      if (newVal.type.type == 'page' || newVal.type.type == 'explain') {
        return
      }
      this.$nextTick(() => {
        let labelIndex = this.labelListAll.indexOf(this.innerItem.content.label)
        this.QuestionTypeColor = labelIndex >= 0 ? this.labelColor[labelIndex] : ''
      })
    },
    addOptions() {
      let length = this.innerItem.content.options.length
      if (length > 25) {
        this.$message.error("选项最多添加26个")
        return
      }
      this.innerItem.content.options.push({
        key: this.letter[length],
        content: ''
      })
      // this.$emit("clickEdit", this.indexItem - 1)
    },
    checkPoints(val) {
      let reg = /^(([1-9]{1}\d*)|(0{1}))(\.\d{0,1})?$/
      if (!reg.test(val)) {
        this.innerItem.content.points = 0
      }
      console.log(val)
      this.$emit('changePoints', true)
    },
    uploadFileCallBack(data) {
      if (data) {
        this.innerItem.content.attachments.push({
          "url": data.url,
          "name": data.name,
        });
        console.log(this.innerItem.content);
        if(data.key === 'file'){

        }
        if (data.key == 'content') {
          this.innerItem.content.content = `${this.innerItem.content.content}[img${this.innerItem.content.attachments.length - 1}]`
        } else {
          this.innerItem.content.options.map(item => {
            if (item.key == data.key) {
              item.content = `${item.content}[img${this.innerItem.content.attachments.length - 1}]`
            }
          })
        }
      }
      this.msgFormSon = ""
    },
    getQueryData() {
      this.exam.unit = utils.getQueryString("unit")
      this.exam.exam = utils.getQueryString("exam")
      this.exam.name = utils.getQueryString("name")
    },
    addAnalysis() {
      this.$emit('analysisAddFn', this.indexItem - 1)
    },
    // closeDialogsure(){
    //     this.closeDialog()
    //     this.$emit('analysisAddFn', this.indexItem - 1)
    // },
  },
  watch: {
    editIndex: {
      handler(newVal) {
        // if((this.indexItem -1) === newVal){
        //     this.editFlag = true
        // }else{
        //     this.editFlag = false
        // }
      },
      immediate: true
    },
    innerItem: {
      handler(newVal) {
        this.changeQuestionType(newVal)
        this.$emit("changeData", this.innerItem, this.indexItem - 1)
      },
      deep: true
    },
    item: {
      handler(newVal) {
        this.innerItem = JSON.parse(JSON.stringify(newVal))
        // this.questionNo = newVal.content.questionNo
        this.changeQuestionType(newVal)
      },
      immediate: true,
      deep: true
    }
  },
  mounted() {
    this.getQueryData()
    EventBus.$on("endDraggbale", (oldIndex) => {
      this.oldIndex = oldIndex
    })
  }
}
