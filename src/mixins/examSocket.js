import io from 'socket.io-client'
import Cookie from 'js-cookie'
export default {
    data(){
        return {
            socket: null
        }
    },
    methods: {
        startSocket(params, fn){
            const url = 'wss://ws.hanlinsys.com/exam?'
            const token = Cookie.get('edu_token')
            const query = Object.assign(params, {
                token
            })
            console.log(query)
            let queryFormat = ''
            for(let item in query){
                queryFormat += `${item}=${query[item]}&`
            }
            this.socket = io(url + queryFormat, {
                transports: ['websocket', 'polling'],
                upgrade: false,
                ...query
            })
            this.socket.on('connect', () => {
                console.log('连接成功')
                fn && fn()
            })
        },
        teacherStartExam(id){
            const params = {
                room: 'Exam-' + id,
                examClass: id
            }
            this.startSocket(params, () => {
                this.teacherlistenStudent()
            })
        },
        teacherlistenStudent(){
            console.log('老师监听学生')
            this.socket.on(`examProgress`, (data) => {
                console.log('老师监听学生', data)
                this.studentProgress = data || {}
            })
        },
        teacherStartSocket(){
            console.log('推送考试socket')
            this.socket.emit(`startExam`, (data) => {
                console.log(data)
                this.$message.success("考试开始成功")
            })
        },
        studentMonitorStartExam(){
            // 学生端初始化socket, 监听考试开始
            console.log('学生端初始化socket, 监听考试开始')
            const params = {
                room: `Exam-${this.result.examClass._id}`,
                stuExam: this.result._id
            }
            this.startSocket(params, (data) => {
                this.socket.emit(`startStuExam-${this.result._id}`, () => {
                    this.startExam()
                })
            })
        },
        studentSendAlarm(){
            // 学生端推送考试告警 
            this.socket.emit(`alarmExam`, {
                stuExam: this.result._id
            }, (data) => {
                console.log(data)
                console.log("推送考试告警")
            })
        },
        studentMonitorAlarm(){
            // 学生端监听考试告警
            this.socket.on(`alarmExam-${this.result._id}`, (data) => {
                console.log(data)
                this.$alert(`您已经累计${data.alarms}次`, '警告', {
                    confirmButtonText: '确定',
                    showClose: false,
                    callback: action => {
                        console.log(data)
                        if(data.forbidden){
                            this.$alert('您已经累计超过了告警最大限制，考试将立即提交', '警告', {
                                confirmButtonText: '确定',
                                showClose: false,
                                callback: action => {
                                    this.submitExam()
                                }
                            })
                        }else{
                            this.fullScreen(this.$refs.examination)
                        }
                    }
                })
            })
        }
    }
}