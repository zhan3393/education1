export default {
    methods: {
        get_task(){
            this.$api.Common.taskCategory(this.exam.course).then(res => {
                if(res.code == 200){
                    this.taskClassification = res.data.list
                    this.exam.category = this.taskClassification.length > 0 ? this.taskClassification[0]._id : ''
                }
            })
        },
    }
}