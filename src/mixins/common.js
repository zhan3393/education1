import utils from '../utils/index'
import Cookie from 'js-cookie'
export default {
    methods: {
        getCurrentData(){
            if(Cookie.get('identity')== 'teacher'){
                this.$api.Teacher.teacher_current().then(res => {
                    this.handle(res)
                })
            }else{
                this.$api.Student.student_current().then(res => {
                    this.handle(res)
                })
            }
        },
        changeTime(time){
            if(!time){
                return ''
            }
            return utils.formatDate(new Date(time), 'yyyy-MM-dd hh:mm:ss')
        },
        handle(res){
            if(res.code == 200){
                this.$store.commit("setUser", res.data)
                window.sessionStorage.setItem('user_id', res.data._id)
            }else{
                this.$message.error(res.error || '获取用户信息失败') 
            }
        },
        getFileName(file){
            let itemArr = file.split("/")
            let name =  decodeURI(decodeURI(itemArr[itemArr.length -1])) 
            return name
        }
    }
}