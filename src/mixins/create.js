import utils from '../utils/index'
export default {
    data(){
        return {
            exam: {
                unit: '',
                course: '',
                name: '',
                startTime: '',
                endTime: '',
                point: '',
                time: '',
                category: 1,
                introduction: '',
                exam: '',
                assignment: ''
            },
            time: {
                start: '',
                end: '',
                timeLong: ''
            },
            disabled: true,
        }
    },
    methods: {
        changeDateTime(val){
            if(val.length > 0){
                this.exam.startTime = val[0]
                this.exam.endTime = val[1]
            }else{
                this.exam.startTime = ''
                this.exam.endTime = ''
            }
        },
        getQueryData(){
            this.exam.unit = utils.getQueryString("unit")
            this.exam.course = utils.getQueryString("course")
            this.exam.name = utils.getQueryString("name")
            this.exam.exam = utils.getQueryString("id") || ''
            this.exam.assignment = utils.getQueryString("assignment") || ''
        },
        openEdit(){
            this.disabled = !this.disabled
            if(this.disabled){
                this.Io.emit("out","out")
            }else{
                this.Io.emit("in","in")
            }
        },
    },
    created(){
        this.getQueryData()
    },
}