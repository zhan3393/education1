import Icon from '../components/pubilc/icon'
import Cookie from 'js-cookie'

export default {
  components: {
    Icon
  },
  props: {
    disabled: {
      default: false,
      type: Boolean
    },
    correct: {
      defalut: false,
      type: Boolean
    },
    correctFlag: {
      defalut: false,
      type: Boolean
    },
    type: {
      default: "",
      type: String
    },
    status: {
      default: 'finish',
      type: String
    }
  },
  data() {
    return {
      currentSrc: '',
      showVisibeFlag: false,
      identity: Cookie.get('identity')
    }
  },
  methods: {
    changeImg(images, content) {
      console.log(images, content)
      if (images !== undefined) {
        if (images.length > 0) {
          images.map((item, index) => {
            content = content.replace(new RegExp(`\\[img${index}\\]`, 'g'), `<img data-img="${item.url.split('?')[0]}" style="width: 50px;cursor: pointer;border: 1px solid #A21832;" src="${item.url.split('?')[0]}"/>`)
          })
        }
      }
      return content
    },
    clickOpen($event) {
      if ($event.target.nodeName == 'IMG') {
        let src = $event.target.dataset.img
        this.currentSrc = src
        this.showVisibeFlag = true
      }
    },
    updateValue() {
      this.$emit("update:answer", this.value)
      this.$emit("submitQuestion", {
        answer: this.value,
        id: this.item.content.stuExamId,
        type: this.item.content.type
      })
    },
    submitStudentScore(val) {
      const params = {
        id: this.item.question._id,
        questions: [{
          _id: this.item.question._id,
        }]
      }
      params.questions[0].teacher_remark = val
      this.$api.Exam.teacher_check_exam(params.id, params).then(res => {
        if (res.code == 200) {
          let flag = true
          this.scoreData.map(item => {
            if (!item.score && item.score != '0') {
              flag = false
            }
          })
          this.currentPreview.status = flag ? 'checked' : 'finished'
        }
      })
    }
  },
  watch: {
    answer: {
      handler(newVal) {
        if (!newVal && this.item.content.type == "Multiple") {
          this.value = newVal || []
        } else {
          this.value = newVal
        }
      },
      deep: true,
      immediate: true
    }
  }
}
