import { resolve, reject } from "q"

export default {
    methods: {
        get_course_classes(course_id){
            let reqData = {
                current: 1,
                pageSize: 1000,
                where: JSON.stringify({'course': course_id})
            }
            return new Promise((resolve, reject) => {
                this.$api.Course.get_courseClass(reqData).then(resData => {
                    if(resData.code == 200){
                        resolve(resData.data.list)
                    }
                })
            })
        },
    }
}