import Io from '../http/socketIo'
export default {
    data(){
        return {
            editCurrent: {},
            editFlag: false,
            editData: [],
            Io: null
        }
    },
    computed: {
        user(){
            return this.$store.state.user
        }
    },
    methods: {
        formatNotice(){
            return "文档正在被用户：{0}({1})编辑，请耐心等待。".format(this.editCurrent.nickname, this.editCurrent.email)
        },
        checkEdit(){
            this.editFlag = false
            this.editCurrent = {}
            this.editData.map(item => {
                if(item.isEdit){
                    this.editCurrent = item
                    if(item._id != this.user._id){
                        this.editFlag = true
                    }
                }
            })
        },
        editIn(data){
            if(!data){
                return
            }
            this.editData.map(item => {
                if(item._id == data._id){
                    item.isEdit = true
                }
            })
            this.checkEdit()
        },
        editLeave(data){
            this.editData = this.editData.filter(item => {
                return item._id != data._id
            })
            this.checkEdit()
        },
        sortTime(){
            let newArr = this.editData.sort((a, b) => b - a)
            return newArr[0]
        },
        editOut(data){
            this.editData.map(item => {
                if(item._id == data._id){
                    item.isEdit = false
                }
            })
            this.checkEdit()
        },
        socket(params = {}){
            this.Io = Io(params)
            this.Io.on('init', data => {
                console.log("---init---",data)
                this.editData = data
                this.checkEdit()
            });
            this.Io.on('join', data => {
                console.log("---join---",data)
                if(data){
                    this.editData.push(data)
                    this.checkEdit()
                }
            })
            this.Io.on('leave', data => {
                console.log("---leave---",data)
                if(data){
                    this.editLeave(data)
                }
            })
            this.Io.on('edit-in', data => {
                console.log('---edit-in---',data)
                if(data){
                    this.editIn(data)
                }
            })
            this.Io.on('edit-out', data => {
                if(data){
                    this.editOut(data)
                }
                console.log('---edit-out---',data)
            })
        }
    }
}