export default {
    methods: {
        checkPassword(password){
            const reg = /^([a-zA-Z0-9]|[,;._!@#$%^&*]){8,16}$/
            if(!reg.test(password)){
                return false
            }
            return true
        },
        checkPhone(phone){
            const reg = /^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$/
            if(!reg.test(phone)){
                return false
            }
            return true
        },
        checkDataNull(data){
            if(data instanceof Array){
                return data.length == 0
            }
            if(data instanceof Object){
                for(let item in data){
                    if(data[item] instanceof Object || data[item] instanceof Array){
                        if(this.checkDataNull(data[item])){
                            return true
                        }
                    }else{
                        if(!data[item]){
                            return true
                        }
                    }
                }
            }
        }
    }
}