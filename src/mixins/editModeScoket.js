import Io from '../http/socketIo'
export default {
    data(){
        return {
            Io: null,
            editData: []
        }
    },
    methods: {
        formatEdit(){
            this.unit_list.map(unit => {
                unit = this.checkType(unit)
                unit.second_level.chapters.map(chapters => {
                    chapters = this.checkType(chapters)
                    chapters.third_level = this.checkType(chapters.third_level)
                })
            })
        },
        checkType(data){
            data.assignments = this.joinEditNow(data.assignments)
            data.coursewares = this.joinEditNow(data.coursewares)
            data.exams = this.joinEditNow(data.exams)
            return data
        },
        joinEditNow(data){
            return data.map(i => {
                i.editUser = null
                this.editData.map(item => {
                    if(i._id == item.type_id){
                        i.editUser = item
                    }
                })
                return i
            })
        },
        formatName(name, editUser){
            return "{0}{1}".format(name, editUser ? '++++----++++【'+ editUser.nickname +'老师正在编辑】' : '')
        },
        editLeave(data){
            this.editData = this.editData.filter(item => {
                return item._id != data._id
            })
            if(data){
                console.log(data)
                this.$notify.closeAll()
                this.$notify({
                    title: '离开提醒：',
                    message: `${data.nickname}(${data.email})离开编辑`,
                    type: 'warning'
                })
            }
            this.formatEdit()
        },
        editIn(data){
            if(data){
                this.editData.push(data)
                this.$notify.closeAll()
                this.$notify({
                    title: '加入提醒：',
                    message: `${data.nickname}(${data.email})加入编辑`,
                    type: 'warning'
                })
                this.formatEdit()
            }
        },
        socket(){
            this.Io = Io({
                course: this.course
            })
            this.Io.on('init', data => {
                //进入初始化，获取编辑人数组
                console.log('---init---',data)
                this.editData = data
                this.formatEdit()
            });
            this.Io.on('join', data => {
                //进入编辑对象数据
                this.editIn(data)
                console.log('---join---',data)
            })
            this.Io.on('leave', data => {
                //离开编辑对象数据
                this.editLeave(data)
                console.log('---leave---',data)
            })
            this.Io.on('edit-in', data => {
                //进入编辑对象数据
                this.editIn(data)
                console.log('---edit-in---',data)
            })
            this.Io.on('edit-out', data => {
                //离开编辑对象数据
                this.editLeave(data)
                console.log('---edit-out---',data)
            })
        }
    }
}