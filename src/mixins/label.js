export default {
    data(){
        return {
            labelList: [],
        }
    },
    methods: {
        clickBack(val){
            this.addLabelVal = val
            if(!val){
                return
            }
            if(this.labelList.length >= 12){
                this.$message("标签最多能创建12个")
                return
            }
            this.labelList.push(val)
            // const params = {
            //     labels: this.labelList
            // }
            // if(this.exam && this.exam.unit){
            //     params.unit = this.exam.unit
            // }
            // this.$api.Exam.update_exam(params, this.exam.exam).then(res => {
            //     if(res.code == 200){
            //         this.$message.success("添加标签成功")
            //         // this.getQuestionLabel()
            //     }else{
            //         this.$message.error("添加失败")
            //         this.labelList.pop()
            //     }
            // })

            this.$nextTick(() => {
                this.addLabelVal = ''
            })
        },
        getQuestionLabel(){
            const params = {
                current: 1,
                pageSize: 999999
            }
            if(this.exam && this.exam.unit){
                params.unit = this.exam.unit
            }
            this.$api.Course.get_uestionLabel(params).then(res => {
                this.labelList = res.data.list
            })
        },
        deleteLabel(index){
            let deleteItem = this.labelList.splice(index, 1)
            if(this.exam.exam){
                this.$api.Exam.edit_exam(this.exam.exam, {
                    labels: this.labelList
                }).then(res => {
                    if(res.code == 200){
                        this.deleteLabelSuccess(deleteItem[0])
                    }
                })
            }
            // if(JSON.stringify(item) != "{}"){
            //     const params = {}
            //     if(this.exam && this.exam.unit){
            //         params.unit = this.exam.unit
            //     }
            //     this.$api.Course.delete_uestionLabel(item._id, params).then(res => {
            //         if(res.code == 200){
            //             this.$message.success("删除标签成功")
            //             this.getQuestionLabel()
            //         }
            //     })
            // }
        }
    }
}