import 'babel-polyfill'
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

// 项目默认使用sass

import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import VueClipboard from 'vue-clipboard2'
import VueAMap from 'vue-amap';
import VueQrcode from '@chenfengyuan/vue-qrcode';
import VideoPlayer from 'vue-video-player'
require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')
Vue.use(VideoPlayer)
// import '../element-variables.scss'
import router from './router'
import store from './store/index'
// import UploadLoad from './http/upload'
// 不使用自定义样式请qing注释下面的内容，并开启默认样式
import '../theme/index.css'
// import 'element-ui/lib/theme-chalk/index.css'

import 'font-awesome/scss/font-awesome.scss'

// 引入自定义样式
import './assets/css/var.scss'
import './assets/css/reset.scss'
import './assets/css/common.scss'

// 全局接口
import api from './http/api.js'
import './utils/prototype.js'

Vue.use(ElementUI)
Vue.use(VueClipboard)
Vue.use(VueAMap)

VueAMap.initAMapApiLoader({
  key: 'f85f4b8c26b83446f437ccbf70e51607',
  plugin: ['AMap.Geolocation', 'AMap.Autocomplete', 'AMap.ToolBar', 'AMap.Geocoder'],
  // 默认高德 sdk 版本为 1.4.4
  v: '1.4.4'
});

Vue.component(VueQrcode.name, VueQrcode);

Vue.config.productionTip = true

Vue.prototype.$api = api

store.$api = Vue.prototype.$api
store.$message = Vue.prototype.$message
store.$confirm = Vue.prototype.$confirm

// Vue.prototype.$upload = UploadLoad
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
